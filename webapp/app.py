import os
from datetime import datetime

from flask import Flask
from psycopg2 import connect

DATABASE_URL = os.environ['DATABASE_URL']
pg_conn = connect(DATABASE_URL)

pg_cursor = pg_conn.cursor()
pg_cursor.execute("create table if not exists visitors (entry varchar(255))")
pg_cursor.close()

app = Flask(__name__)


@app.route('/')
def index():
    pg_cursor = pg_conn.cursor()
    insert_entry = "insert into visitors (entry) values ('{}')".format(datetime.now())
    pg_cursor.execute(insert_entry)
    select_entries = "select * from visitors"
    pg_cursor.execute(select_entries)
    visitors_list = pg_cursor.fetchall()
    html_response = '<h2>List of Visitors</h2>'
    html_response += '<ul>'
    for row in visitors_list:
        html_response += '<li>' + row[0] + '</li>'
    html_response += '</ul>'
    pg_cursor.close()
    return html_response
